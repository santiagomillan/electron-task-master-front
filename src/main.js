const { app, BrowserWindow, Menu, ipcMain } = require("electron");
const url = require("url");
const path = require("path");

const { crearVentanaNuevaTarea } = require("./templates/nuevaTarea");

// Electron reload en modo desarrollo
if (process.env.NODE_ENV !== "production") {
  require("electron-reload")(__dirname, {});
}

let ventanaPrincipal;
let ventanaNuevaTarea;

// Ready => Aplicación está lista
app.on("ready", () => {
  // Crea la ventana principal
  ventanaPrincipal = new BrowserWindow({
    webPreferences: {
      nodeIntegration: true,
    },
  });
  // Crea la url de la ventana
  let urlVentana = url.format({
    //pathname: path.join(__dirname, "views/index.html"),
    pathname: path.join(__dirname, "views/bookmark.html"),
    protocol: "file",
    slashes: true,
  });

  // Carga el menu de un template
  const menuPrincipal = Menu.buildFromTemplate(templateMenu);

  // Asigna el menú creado a la aplicación
  Menu.setApplicationMenu(menuPrincipal);

  // Crea la ventana principal cargando un HTML
  ventanaPrincipal.loadURL(urlVentana);

  // Cuando se cierre la ventana principal, se cierra al aplicación
  ventanaPrincipal.on("closed", () => {
    app.quit();
  });

  // Escucha el evento para la cración de una nueva tarea
  ipcMain.on("nueva-tarea", (evento, datos) => {
    // Enviar el mensaje a la ventana principal
    ventanaPrincipal.webContents.send("nueva-tarea", datos);
    // Cierra la ventana
    ventanaNuevaTarea.close();
  });
  
});

const templateMenu = [
  {
    label: "Tareas",
    submenu: [
      {
        label: "Nueva tarea",
        accelerator: "Ctrl+N",
        click() {
          // Crear la ventana de crear tarea
          ventanaNuevaTarea = crearVentanaNuevaTarea();
        },
      },
      {
        label: "Salir",
        accelerator: process.platform === "darwin" ? "command+Q" : "Ctrl+Q",
        click() {
          app.quit();
        },
      },
    ],
  },
];

// Vefifica si el sistema operativo es Mac os
if (process.platform === "darwin") {
  templateMenu.unshift({ label: app.getName() });
}

// Activa/desactiva el Dev Tools
if (process.env.NODE_ENV !== "production") {
  templateMenu.push({
    label: "DevTools",
    submenu: [
      {
        label: "Mostrar u ocultar Dev Tools",
        click(item, focusedWindow) {
          focusedWindow.toggleDevTools();
        },
      },
      {
        role: "reload",
      },
    ],
  });
}
/*const { app, BrowserWindow, Menu, ipcMain } = require("electron");
const url = require("url");
const path = require("path");

const {crearVentanaNuevaTarea} = require("./templates/nuevaTarea");
// Electron reload en modo desarrollo
if (process.env.NODE_ENV !== "production") {
  require("electron-reload")(__dirname, {});
}

let ventanaPrincipal;
let ventanaNuevaTarea;

// Ready => Aplicación está lista
app.on("ready", () => {
  // Crea la ventana principal
  ventanaPrincipal = new BrowserWindow({
    webPreferences: {
      nodeIntegration: true,
    },
  });
  // Crea la url de la ventana
  let urlVentana = url.format({
    pathname: path.join(__dirname, "views/index.html"),
    protocol: "file",
    slashes: true,
  });

  // Carga el menu de un template
  const menuPrincipal = Menu.buildFromTemplate(templateMenu);

  // Asigna el menú creado a la aplicación
  Menu.setApplicationMenu(menuPrincipal);

  // Crea la ventana principal cargando un HTML
  ventanaPrincipal.loadURL(urlVentana);

  // Cuando se cierre la ventana principal, se cierra al aplicación
  ventanaPrincipal.on("closed", () => {
    app.quit();
  });

  // Escucha el evento para la cración de una nueva tarea
  ipcMain.on("nueva-tarea", (evento, datos) => {
    console.log(datos);
//envia mensaje a v principal
    ventanaPrincipal.webContents.send("nueva-tarea", datos);
    // cerrar ventana
    ventanaNuevaTarea.close();
  });
});

const templateMenu = [
  {
    label: "Tareas",
    submenu: [
      {
        label: "Nueva tarea",
        accelerator: "Ctrl+N",
        click() {
          // Crear la ventana de crear tarea
          crearVentanaNuevaTarea();
        },
      },
      {
        label: "Salir",
        accelerator: process.platform === "darwin" ? "command+Q" : "Ctrl+Q",
        click() {
          app.quit();
        },
      },
    ],
  },
];

// Vefifica si el sistema operativo es Mac os
if (process.platform === "darwin") {
  templateMenu.unshift({ label: app.getName() });
}

// Activa/desactiva el Dev Tools
if (process.env.NODE_ENV !== "production") {
  templateMenu.push({
    label: "DevTools",
    submenu: [
      {
        label: "Mostrar u ocultar Dev Tools",
        click(item, focusedWindow) {
          focusedWindow.toggleDevTools();
        },
      },
      {
        role: "reload",
      },
    ],
  });
}

function crearVentanaNuevaTarea() {
  // Crea la ventana para crear una nueva tarea
  ventanaNuevaTarea = new BrowserWindow({
    width: 800,
    height: 250,
    title: "Nueva Tarea",
    webPreferences: {
      nodeIntegration: true,
    },
  });

  // Elimina el menú de la ventana para crear tarea
  ventanaNuevaTarea.setMenu(null);

  // Crea la url de la ventana
  let urlVentana = url.format({
    pathname: path.join(__dirname, "views/nuevaTarea.html"),
    protocol: "file",
    slashes: true,
  });
  // Carga el HTML
  ventanaNuevaTarea.loadURL(urlVentana);

  // Libera memoria cuando se cierra la ventana
  ventanaNuevaTarea.on("closed", () => {
    ventanaNuevaTarea = null;
  });
}


/*const { app, BrowserWindow, Menu, BrowserWindowProxy, ipcMain  } = require("electron");
const url =require("url");
const path = require("path");
const { Console } = require("console");

// Electron reload en modo desarrollo
if (process.env.NODE_ENV !== "production") {
    require("electron-reload")(__dirname, {});
  }

let ventanaPrincipal;
let ventanaNuevaTarea;

app.on("ready", () => {
    ventanaPrincipal = new  BrowserWindow({
        webPreferences:{
            nodeIntegration: true,
        },
    });
    
    let urlVentana = url.format({
        pathname: path.join(__dirname , "views/index.html"),
        protocol: "file",
        slashes: true,
    });

    const menuPrincipal = Menu.buildFromTemplate(templateMenu);
    Menu.setApplicationMenu(menuPrincipal);
    ventanaPrincipal.loadURL(urlVentana);
    ventanaPrincipal.on("closed" , ()=>{
        app.quit();
    });

    ipcMain.on("nueva-tarea",(evento , datos) =>{
        console.log(datos);
    });

});

const templateMenu = [
    {
        label: "Tareas",
        submenu:[
            {
                label:"Nueva tarea",
                acelerator:  process.platform === 'darwin' ? 'command+N' : "Ctrl+N",
                click(){
                    //console.log("Click")
                    crearVentanaNuevaTarea();
                },
            },
            {
                label: "Salir",
                acelerator: process.platform === 'darwin' ? 'command+Q' : "Ctrl+Q",
                click(){
                    app.quit();
                    
                }
            }
        ],
    },
];

//verifica SO
if(process.platform === "darwin"){
    templateMenu.unshift({label: app.getName()});
}
// Activa/desactiva el Dev Tools
if (process.env.NODE_ENV !== "production") {
    templateMenu.push({
      label: "DevTools",
      submenu: [
        {
          label: "Mostrar u ocultar Dev Tools",
          click(item, focusedWindow) {
            focusedWindow.toggleDevTools();
          },
        },
        {
          role: "reload",
        },
      ],
    });
  }

//crear ventana
function crearVentanaNuevaTarea() {
    // Crea la ventana para crear una nueva 
    ventanaNuevaTarea = new BrowserWindow({
       width: 400,
       height: 250,
       title: "Nueva Tarea",
    });
    //ventanaNuevaTarea.setMenu(null);
    // Crea la url de la ventanalet 
    urlVentana = url.format({
        pathname: path.join(__dirname, "views/nuevaTarea.html"),
        protocol: "file",
        slashes: true,
    });
        // Carga el HTML
    ventanaNuevaTarea.loadURL(urlVentana);
    //livera memoria
    ventanaNuevaTarea.on("closed", () =>{
        ventanaNuevaTarea = null;
    });

}*/

