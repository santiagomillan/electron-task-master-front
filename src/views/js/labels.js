document.addEventListener("DOMContentLoaded",() => {

    let boton = document.getElementById("boton")

    boton.addEventListener("click",() =>{
        //console.log("me tocaron")
        let tarea = document.getElementById("tarea");
        console.log(tarea.value);
        let ul = document.getElementById('lista')
        let li = document.createElement('li');
       // li.textContent = tarea.value;

        let checkbox = document.createElement("input");
        checkbox.setAttribute("type", "checkbox");

        checkbox.addEventListener("click", clickcheckbox);
        let nodoTexto = document.createTextNode(tarea.value);
        let botoneliminar = document.createElement("button");
        botoneliminar.textContent='x';
        botoneliminar.classList.add("boton-eliminar");
        botoneliminar.addEventListener("click" , clickbutoneliminar);


        //let nodoTexto = document.createTextNode(tarea.value);
        li.append(checkbox  , nodoTexto, botoneliminar);

        ul.appendChild(li);
        tarea.value = ""
    });
});

function clickcheckbox(evento){
    //console.log(evento);    
    let elemento = evento.target;
    let lipadre = elemento.parentElement;
    //console.log(lipadre)
    lipadre.classList.toggle("terminada")
}
function clickbutoneliminar(evento){
    let elemento = evento.target;
    let lipadre = elemento.parentElement;
    lipadre.remove()
}