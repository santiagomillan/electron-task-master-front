function filtrar() {
    //Se crrean las variables y se obtienen los elementos
    var input, filter, table, tr, td, i, txtValue;
    input = document.getElementById("input");
    filter = input.value.toUpperCase();
    table = document.getElementById("tabla");
    tr = table.getElementsByTagName("tr");
    // Se recorre la tabla
    for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[1];
        if (td) {
            txtValue = td.textContent || td.innerText;
            // Coloca los datos en mayusculas para buscar
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }
    }
}

function datoentrada() {
    
    // Se capturan los datos
    var input = document.getElementById("nombre").value;
    var input2 = document.getElementById("label").value;
    var url1 = document.getElementById("url").value;
    var comentario =  document.getElementById("comentario").value;
    var login = document.getElementById("correo").value
    var password = document.getElementById("contraseña").value
    var boton1 = document.createElement("label");

    //se guardan los datos en un arreglo
    const DATOS = [
        { nombres: input, label: input2, url: url1, comentario: comentario, login : login, password : password,}
    ];
    // Obteniendo el cuerpo de la tabla a donde añadiremos nuestros datos
    let tabla = document.getElementById('tabla');

    // Recorriendo los datos de ejemplo
    for (let i = 0; i < DATOS.length; i++) {
        // Se crean los td
        let name = `<td class="td">${DATOS[i].nombres}</td>`;
        let url = `<td class="td">${DATOS[i].url}</td>`;
        let label = `<td class="td">${DATOS[i].label}</td>`;
        let boton = `<td id="td" "></td>`;
        // se agregan los tds a un tr 
        tabla.innerHTML += `<tr id="tr">${name + label + url  + boton}</tr>`;
        let tr = document.getElementById("tr")
        //se agrega boton eliminar al nuevo bookmark
        var h = document.getElementById("td");
        h.insertAdjacentHTML("afterbegin", "<button class='botonEliminar' onclick='eliminarbook(this)'>X</button>");
    }
    nombre = input;
    console.log(nombre)
    const axios = require('axios')

    async function crearBookmark() {
      try {
        // Hace la petición al Rest API
        let respuesta = await axios.post("http://localhost:3000/bookmarks",
          {
            nombre: input,
            url: url1,
            comentario: comentario,
            labels: input2,
            login: login,
            password: password,
          });
        // Muestra el resultado
        console.log('correcto') 
        console.log(respuesta.data);
      } catch (error) {
        console.log(error);
      }
    }
    crearBookmark()
}
function eliminarbook(id) {
    //Se obtiene los padres
    var columna = id.parentNode;
    var fila = columna.parentNode;
    // Se elimina la fila correspondiente
    document.getElementById("tabla").deleteRow(fila.rowIndex);
}

// // Obtiene el elemento
// let elemento = document.getElementById("tr");
// // Elimina el elemento
// elemento.remove();


