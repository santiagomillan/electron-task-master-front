const axios = require('axios')

// Crea una tarea
async function crearTarea() {
  try {
    // Hace la petición al Rest API
    let respuesta = await axios.post("http://localhost:3000/tareas", 
    {
      titulo: "esta es una nueva tarea",
      descripcion:"Esta es una descripcion"
    });
    // Muestra el resultado
    console.log(respuesta.data);
  } catch (error) {
    console.log(error);
  }
}

crearTarea();   